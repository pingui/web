// Requires
const db = require('./db')

// Validate Token
const validateToken = (tableName, token) => {
  return new Promise((resolve, reject) => {
    db.get(`SELECT id FROM ${tableName} WHERE token = ?`, [token], (err, row) => {
      if (err) {
        reject(err)
      } else {
        if (row === undefined) {
          reject("El token no s'ha trobat. Assegura't que l'has introduït correctament. En cas de ser així, potser ja s'ha confirmat")
        } else {
          db.run(`UPDATE ${tableName} SET token = NULL WHERE token = ?`, [token], (err) => {
            if (err) {
              reject(err)
            } else {
              resolve("S'ha confirmat la inscripció correctament!")
            }
          })
        }
      }
    })
  })
}

module.exports = validateToken

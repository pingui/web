// Requires
const express = require('express')
const router = express.Router()
const validateToken = require('../methods/validateToken')

// GET: /forms/validate/:table/:token; Validate a request through the token
router.get('/:table/:token', (req, res) => {
  if ((req.params.table === 'ponencies') || (req.params.table === 'inscripcions')) {
    validateToken(req.params.table, req.params.token).then((result) => {
      res.send(result)
    }, (result) => {
      res.send('Hi ha hagut un error validant el teu token! ' + result)
    })
  }
})

module.exports = router

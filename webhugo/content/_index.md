![](logos/obrimelcoco_50px.png)

## 1a Jornada de Programari Lliure als Centres Educatius

#### 27 de Juny de 2018 a l'Escola del Treball de Barcelona

Som molts, cada vegada més, els educadors que triem sempre que podem el programari lliure pels valors transversals que aporta en l'entorn del coneixement.

L'objectiu de la trobada és compartir diferents experiències i iniciatives relacionades amb el programari lliure orientades a l'educació. Volem reunir a persones de diferents centres educatius i organitzacions que estan interessades a promoure les tecnologies lliures a l'àmbit educatiu. L'esdeveniment és gratuït i et pots inscriure online.

- Formulari d'inscripció: **[Inscripcions](https://obrimelcodi.cat/inscripcio/)**

Tens experiències amb programari lliure en l'entorn educatiu? Vine i comparteix-la amb la comunitat educativa.

- Formulari de proposta de ponència (fins el 17 de juny): **[Propostes de ponència](https://obrimelcodi.cat/inscripcions_ponencies/)**

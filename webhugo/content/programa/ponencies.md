---
title: "Ponències"
date: 2018-05-29T11:29:49+02:00
draft: false
#layout: prova
---

## Ponències

A mesura que anem rebent i confirmant ponències les anirem afegint al programa de la Jornada. 

Propostes de 5 minuts

- 14 ponències
- 5 minuts per ponència
- 2 minuts de marge entre ponències (canviar presentació, conectar projector, alguna pregunta concreta...)

| HORA  | TÍTOL | PONENT |
| ----- | ----- | ------ |
| 10:00 |       |        |
| 10:07 |       |        |
| 10:15 |       |        |
| 10:22 |       |        |
| 10:30 |       |        |
| 10:37 |       |        |
| 10:45 |       |        |
| 10:52 |       |        |
| 11:00 |       |        |
| 11:07 |       |        |
| 11:15 |       |        |
| 11:22 |       |        |
| 11:30 |       |        |
| 11:37 |       |        |

Propostes de 15 minuts

- 6 ponències
- 15 minuts per ponència
- Si voleu deixar temps per preguntes escurseu els 15 minuts de presentació.

| HORA  | TÍTOL | PONENT |
| ----- | ----- | ------ |
| 12:15 |       |        |
| 12:30 |       |        |
| 12:45 |       |        |
| 13:00 |       |        |
| 13:15 |       |        |
| 13:30 |       |        |

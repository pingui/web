---
title: "Horari"
date: 2018-05-29T11:29:49+02:00
draft: false
layout: prova
---

## Horari de la jornada

### Dimecres 27 de juny de 2018

**09:30h** Acollida

**10:00h** [Ponències](../ponencies)

**11:45h** Pausa café

**12:15h** Ponències

**13:45h** Finalització Jornades

**14:00h** Dinar (12 euros cal apuntar-se abans)
